# -*- coding: utf-8 -*-

from trytond.pool import Pool
from .health_sex_reproductive_program import *
from .report import *


def register():
    Pool.register(
        SaludSexual,
        LecheMaternaMadres,
        LecheMaternaMenores,
        ExtraPrograma,
        module='health_sex_reproductive_program_fiuner', type_='model')
    Pool.register(
        ReporteSaludSexual,
        module='health_sex_reproductive_program_fiuner', type_='report')

